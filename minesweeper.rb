require 'debugger'
require 'yaml'

class Minesweeper

  attr_accessor :board, :player

  def initialize
    @board = Board.new(9)
    @player = Player.new
    play
  end

  def play
    until blown_up? || won?
      puts "'1' to set flag, '2' to check mine, '3' to save, '4' to load"
      choice = gets.chomp.to_i

      coordinates = []
      if choice == 1 # they want to set flag
        coordinates = player.get_move
        board.set_flag(coordinates)
      elsif choice == 2 # they want to check mine
        coordinates = player.get_move
        board.reveal_space(coordinates)
      elsif choice == 3
        save_game
        puts "You saved the game"
        break
      elsif choice == 4
        load_game
      end
      board.display_board
    end

    print "You lose, you loser" if blown_up?
    print "You win, you're a pretty great human/dog???" if won?
  end

  def blown_up?
    9.times do |row|
      9.times do |col|
        if board.board[row][col].revealed && board.board[row][col].bomb
          return true
        end
      end
    end

    false
  end

  def won?
    9.times do |row|
      9.times do |col|
        if !(board.board[row][col].revealed) && !(board.board[row][col].bomb)
          return false
        end
      end
    end
  end

  def save_game
    File.open('minesweeper_game', 'w') do |f|
      f.puts self.to_yaml
    end
  end

  def load_game
    game2  = YAML::load(File.read('minesweeper_game'))
    game2.play
  end
end

class Board
  attr_accessor :board
  attr_reader :size

  def initialize(size)
    @board = build_board(size)
    @size = size
    fill_spaces
    set_bombs(size)
    set_adj_bombs
  end

  def build_board(size)
    if size == 9
      board_arr = Array.new(size) { Array.new(size) }
    end
  end

  def fill_spaces
    size.times do |row|
      size.times do |col|
        board[row][col] = Space.new
      end
    end
  end

  def set_bombs(size)
    if size == 9
      bombs = 10
      until bombs == 0
        x, y = (0..8).to_a.sample, (0..8).to_a.sample
        if board[x][y].bomb
          next
        else
          board[x][y].bomb = true
          bombs -= 1
        end
      end
    end
  end

  def display_board
    #set_board
    board.flatten.each_with_index do |space, i|
      space.set_display
      print space.display
      puts if (i + 1) % 9 ==0
    end

  end


  def set_adj_bombs
    9.times do |row|
      9.times do |col|
        board[row][col].adj_bombs = check_neighbors([row, col])
      end
    end
  end

  def check_neighbors(cell)
    # helper method for set_adj_bombs
    # should only run once
    x, y = cell[0], cell[1]
    bomb_count = 0
    neighbors_arr = get_neighbors([x,y])

    neighbors_arr.each do |neighbor|
      if board[neighbor[0]][neighbor[1]].bomb
        bomb_count += 1
      end
    end

    bomb_count
  end

  def get_neighbors(coordinates)
    x, y = coordinates[0], coordinates[1]
    neighbors_arr = []

    (-1..1).each do |x_offset|
      (-1..1).each do |y_offset|
        #next if [x_offset, y_offset] == [0, 0]
        next if (x+x_offset) > 8 || (x+x_offset) < 0
        next if (y+y_offset) > 8 || (y+y_offset) < 0
        neighbors_arr << [(x+x_offset),(y+y_offset)]
      end
    end

    neighbors_arr # [[1,2],[2,2],[3,3]]
  end

  def reveal_space(coordinates)
    space = board[coordinates[0]][coordinates[1]]

    return true if space.revealed

    space.reveal

    # debugger
    if space.adj_bombs == 0
      neighbors = get_neighbors(coordinates)
      neighbors.each do |neighbor|
        reveal_space(neighbor)
      end
    end
  end

  def set_flag(coordinates)
    space = board[coordinates[0]][coordinates[1]]

    space.flag_it
  end
end

class Space

  attr_accessor :bomb, :revealed, :flagged, :display, :adj_bombs

  def initialize
    @bomb = false
    @revealed = false
    @flagged = false
    @adj_bombs = 0
    set_display
    @display = "*"
  end

  def set_display
    if bomb
      if revealed
        self.display = "B"
      elsif flagged
        self.display = "F"
      else #unrevealed
        self.display = "*"
      end
    else #not bomb
      if revealed
        if adj_bombs == 0
          self.display = "_"
        else
          self.display = "#{adj_bombs}"
        end
      else #unrevealed
        if flagged == false
          self.display = "*"
        else
          self.display = "F"
        end
      end
    end
  end

  def reveal
    self.revealed = true
    set_display
  end

  def flag_it
    self.flagged = true
    set_display
  end

end

class Player
  def initialize
  end

  def get_move
    puts "Give x-coordinates: "
    x = gets.chomp.to_i
    puts "Give y-coordinates: "
    y = gets.chomp.to_i
    [x,y]
  end
end

game = Minesweeper.new
game